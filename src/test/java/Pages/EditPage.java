package Pages;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;


public class EditPage extends Annotations {
	//@Given("Edit Company name (.*)")
	@Given("Edit Company name")
	public EditPage typeCompanyName() {
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Infosys");
		return this;
	}
	
	
	

	@Given("Click Update")
	public ViewLeadPage clickUpdateLeadButton() {
		driver.findElementByXPath("//input[@value='Update']").click();
		return new ViewLeadPage();
	}
	
	
	
	/*
	 * @Given("Enter First name (.*)") public EditPage typeFirstName(String data) {
	 * driver.findElementById("createLeadForm_firstName") .sendKeys(data); return
	 * this; }
	 * 
	 * 
	 * @Given("Enter Last name (.*)") public EditPage typeLastName(String data) {
	 * driver.findElementById("createLeadForm_lastName") .sendKeys(data); return
	 * this; }
	 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
