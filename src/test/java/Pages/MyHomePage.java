package Pages;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import com.leafBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	@When("Click Leads tab")
	public MyLeadsPage clickLeadsTab() {
		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}

}
