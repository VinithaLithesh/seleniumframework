package Pages;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import com.leafBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	@Given("Click Create")
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
	}
	
	
public MyLeadsPage clickDeleteLead() {
	
driver.findElementByLinkText("Delete").click();
return new MyLeadsPage();
}


@When("Click Find Lead")
public FindLeadsPage clickFindLead() {
	driver.findElementByLinkText("Find Leads").click();
	return new FindLeadsPage();

}
@When("Click Merge Leads")
public MergeLeadPage clickMergeLead() {
	driver.findElementByXPath("//a[text()='Merge Leads']").click();
	return new MergeLeadPage();
}

}

