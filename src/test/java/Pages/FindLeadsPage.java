package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.leafBot.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations {
	
	@When("Enter First Name")
	public  ViewLeadPage findLeadByName(String fName) {
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input[2]").sendKeys(fName);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Actions act = new Actions(driver);
		
		WebElement list = driver.findElementByXPath("(//a[@class='linktext'])[4]");
		
		act.moveToElement(list).click().perform();
		return new ViewLeadPage();
	

	}
	
	@When("Enter First Name to Edit")
	public  ViewLeadPage findLeadByNametoEdit() {
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input[2]").sendKeys("malar");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Actions act = new Actions(driver);
		
		WebElement list = driver.findElementByXPath("(//a[@class='linktext'])[4]");
		System.out.println(list.getText());
		act.moveToElement(list).click().perform();
		return new ViewLeadPage();
	

	}

}
