package Pages;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;


public class CreateLeadPage extends Annotations {
	
	
	
	@Given("Enter Company name (.*)")
	public CreateLeadPage typeCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName")
		.sendKeys(data);
		return this;
	}
	
	@Given("Enter First name (.*)")
	public CreateLeadPage typeFirstName(String data) {
		driver.findElementById("createLeadForm_firstName")
		.sendKeys(data);
		return this;
	}
	

	@Given("Enter Last name (.*)")
	public CreateLeadPage typeLastName(String data) {
		driver.findElementById("createLeadForm_lastName")
		.sendKeys(data);
		return this;
	}
	

	@Given("I click create Lead")
	public ViewLeadPage clickCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
