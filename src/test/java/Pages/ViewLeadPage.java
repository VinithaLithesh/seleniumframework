package Pages;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.leafBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	@When("Verify Create lead is success")
	
	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
		}
	
@Then("Verify Duplicate lead is success")
	
	public ViewLeadPage verifyDuplicateFirstName(String list) {
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		if(text.equals(list)) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
		}
		
	public FindLeadsPage clickFindLeads() {
           driver.findElementByLinkText("Find Leads").click();
           return new FindLeadsPage();
		
	}
	
	@When("Click Edit")
	public EditPage clickEdit() {
		driver.findElementByLinkText("Edit").click();
		return new EditPage();
	}
	@When("Click Duplicate Lead")
	public DuplicateLeadPage clickDuplicateLead() {
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeadPage();

	}
	
@Then("Verify Merge lead is success")
	
	public ViewLeadPage verifyMergeLead() {
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(text);
		return this;
		}
	
	}

	
	
	
	
	
	

